# iAD native extension for Rhodes framework

This extension is a glue of rhodes app, Rhodes framework and iAD framework, so your app can display iAD banner on the view.

You need some extra procedures to use the extension, because it's depend on Rhodes framework tightly.

## Quick Start
This section describes how to build a sample app with the extension.

###1. get the extension resource

Clone a repository of the extension in terminal,

	$ git clone git@bitbucket.org:ackcell/iadextentionforrhodes.git

or

[download snapshot](https://bitbucket.org/ackcell/iadextentionforrhodes/downloads) and extract archive file.

###2. determinate Rhodes framework to build you app

If you installed the framework already, find location where the framework installed. Typically,

* /usr/lib/ruby/gems/1.8/gems/rhodes-x.x.x # when system ruby
* /Users/$USER/.rvm/gems/ruby-1.8.7-p352/gems/rhodes-x.x.x # when rvm

or somewhere you installed.

Assume that $RHODESDIR is the directory.

###3. set rhodes framework

'cd' to cloned directory(or extracted download snapshot directory) of the extension, then 'set-rhodes-sdk' in Terminal.

	$ cd iadextension
	$ set-rhodes-sdk
	$RHODESDIR/bin
	$

###4. prepare to build by Xcode

You need a couple of rake command to build your app by Xcode. Enter 

* rake switch\_app

 and
 
* rake build:iphone:setup\_xcode\_project

 commands in the cloned directory.


	$ rake switch_app
	$app_config['extensions'] : ["json", "another-extension"]
	$app_config['capabilities'] : []
	
	$ rake build:iphone:setup_xcode_project
	$app_config['extensions'] : ["json", "another-extension", "iadnetwork", "fileutils", "mspec"]
	$app_config['capabilities'] : []
	iphonesimulator5.0
	Check SDK :
	PWD: /Users/$USER/.rvm/gems/ruby-1.8.7-p352/gems/rhodes-x.x.x 
	CMD: /Developer/usr/bin/xcodebuild -version -sdk iphonesimulator5.0
	RET: iPhoneSimulator5.0.sdk - Simulator - iOS 5.0 (iphonesimulator5.0)
	RET: SDKVersion: 5.0
	RET: Path: /Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator5.0.sdk
	RET: PlatformPath: /Developer/Platforms/iPhoneSimulator.platform
	RET: ProductBuildVersion: 9A334
	RET: ProductCopyright: 1983-2011 Apple Inc.
	RET: ProductName: iPhone OS
	RET: ProductVersion: 5.0
	RET: iPhoneSimulator5.0.sdk - Simulator - iOS 5.0 (iphonesimulator5.0)
	SDKVersion: 5.0
	Path: /Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator5.0.sdk
	PlatformPath: /Developer/Platforms/iPhoneSimulator.platform
	ProductBuildVersion: 9A334
	ProductCopyright: 1983-2011 Apple Inc.
	ProductName: iPhone OS
	ProductVersion: 5.0
	
	set bundle identifier
	set name
	set name
	set URL Scheme
	set URL name
	set icon
	set_default_images
	$


###5. open sample app project file with Xcode

    $ open $RHODESDIR/platform/iphone/rhorunner.xcodeproj/


Add iAD.framework into the project.

* select 'Project navigator' and click 'rhorunner' on top of the nagivator.
* select 'Build Phases', click 'Link Binary With Libraries', click '+' then add iAD.framework.
* change 'Required' to 'Optional', if you want building app to work on iOS4.0 prior, even no iAD functionality. (iAD supports iOS4.0 or later)

###6. build and run

The built sample app has three items on screem.

* iAD API list: you can try all APIs.
* typically case: follow sequence of list. It's similar calling API sequence of typically app. 
* Run tests: Unit tests for the native extension.

## API
You should have 'require' and 'include' in your controller.


    require 'iadnetwork'
    Class Xxxxxx < 
      include Iadnetwork
    
      def ...
      end
    end


Your controller can call following APIs.

###start

    Iadnetwork.start(requiredContentSizeIdentifiers)

start iAD. Specify required content size

* requiredContentSizeIdentifiers:

1. iad_both

2. iad\_portrait\+iad\_landscape:same as iad_both

3. iad\_portrait

4. iad\_landscape

start function will not show iAD. Call explicitly show API to display iAD on the view.

###setPos(*)

    Iadnetwork.setPos(x,y)


set iAD banner location.

###setSize(*)

    Iadnetwork.setSize(currentContentSizeIdentifier)

set current content size

* currentContentSizeIdentifier

1. iad_portrait
2. iad_landscape

###show(*)

    Iadnetwork.show

show iAD banner on the view when iAD is available. The extension will hide an empty iAD banner automatically, when iAD is not available.

###hide

    Iadnetwork.hide

hide iAD banner, even iAD is available.

###quit

    Iadnetwork.quit

terminate iAD. If your app needs iAD no longer in its life cycle, call quit to release resource.

###NOTE
 
If you want to hide iAD due to app circumstance, call Iadnetwork.hide instead. hide->show is more efficient rather than quit->start->show.

(\*): If you don't call start, then call some APIs marked \*, start(iad_both) will be invoked automatically.


## Run Time Configuration

You can configure the extension, when you edit a rhoconfig.txt of your app.

    # iad exntension
    # EXiadDebug = 1     # uncomment when you want the exntension display debug message.
    # EXiadNoAnimation = 1    # uncomment when you want to suppress ad banner animation. 

## REMARK

* iAD native extension works on iOS device and iOS simulator only.
* RhoSimulator is NOT supported as well.
* To provide iAD, iOS 4.0 or later required.
* Built app with the extension will work on iOS 3.x, if you link iAD.framework as weak link(optional link).
* The extension can NOT place individual iAD banner for each view in Native Multi Tab app.


## Import

This section describes how to import iadnetwork extension into your app.

### 1. get the extension resource
Clone a repository of the extension in terminal,

    $ git clone git@bitbucket.org:ackcell/iadextentionforrhodes.git

or

[download snapshot](https://bitbucket.org/ackcell/iadextentionforrhodes/downloads) and extract archive file.

Assume that $IADEXDIR is the directory of cloned or extracted.

### 2. copy the extension folder into your app folder

From cloned or downloaded extension, copy extension/iadextension folder into extension folder of your app.

    $ cd <your rhodes app folder>
    $ cp -r $IADEXDIR/extension/iadextension extension

### 3. determinate Rhodes framework to build you app

If you installed the framework already, find location where the framework installed. Typically,

* /usr/lib/ruby/gems/1.8/gems/rhodes-x.x.x # when system ruby
* /Users/$USER/.rvm/gems/ruby-1.8.7-p352/gems/rhodes-x.x.x # when rvm

or somewhere you installed. You may find location in build.yml of your app. i.e.

    sdk: /usr/lib/ruby/gems/1.8/gems/rhodes-3.3.3

or

    sdk: /Users/$USER/.rvm/gems/ruby-1.8.7-p352/gems/rhodes-3.3.3

Assume that $RHODESDIR is the directory.

### 4. set rhodes framework

Enter 'set-rhodes-sdk' in your app directory, or you set it already.

    $ set-rhodes-sdk
    $RHODESDIR/bin

### 5. edit build.yml

To tell the framework that app needs the iadnetwork extension, add "iadnetwork" in extensions section of build.yml.

    iphone:
    extensions:
    - iadnetwork

### 6. prepare to build by Xcode

You need a couple of rake command to build your app by Xcode. Enter 

* rake switch\_app
 
* rake build:iphone:setup\_xcode\_project

 commands in your app directory.

    $ rake switch_app
    $ rake build:iphone:setup_xcode_project

### 7. open sample app project file with Xcode

    $ open $RHODESDIR/platform/iphone/rhorunner.xcodeproj/

Add iAD.framework into the project.

* select 'Project navigator' and click 'rhorunner' on top of the nagivator.
* select 'Build Phases', click 'Link Binary With Libraries', click '+' then add iAD.framework.
* change 'Required' to 'Optional', if you want building app to work on iOS4.0 prior, even no iAD functionality. (iAD supports iOS4.0 or later)

### 8. build and run

If you can build a rhodes app with the extension, it's time to code to implement for iAD.

You will find some example code at $IADEXDIR/app.

* $IADEXDIR/app/IadnetworkTest
* $IADEXDIR/app/IadnetworkTypically
* $IADEXDIR/app/test
