/*
 * Copyright (c) 2012 HAYAKAWA Takashi
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * http://ackcell.com
 */
#import "Iadnetwork.h"
#include "ruby/ext/rho/rhoruby.h"
#define UIApp [UIApplication sharedApplication]

// Reference this Objective C class.
Iadnetwork *iadn = nil;

static BOOL iadn_valid_os = YES;

static BOOL animation = YES;
static BOOL debug = NO;
static id adsdelegate = nil;

static id<NSObject> mainView;

@interface Iadnetwork()
-(void)   start:(int) _size;
-(void) unthread_start;
-(void) displayad;
-(void) hidead;
-(void) setAttr:(int) ori;
-(void) unthread_setPosition;
-(void) unthread_setAttr;

@end

@implementation Iadnetwork
@synthesize adView;
@synthesize allowActionToRun;
@synthesize isAvailable;
@synthesize isVisible;
@synthesize on;
@synthesize x;
@synthesize y;
@synthesize adSize;
@synthesize reqAdSize;
//
#pragma MARK iAD core
//
- (id)init {
    UIApplication *app = (UIApplication *)[UIApp delegate];
    mainView = [app mainView];

    adView = nil;
    isAvailable = NO;
    isVisible = NO;
    on = NO;
    allowActionToRun = YES;
    x = 0;
    y = 0;
    reqAdSize = EXIAD_BOTH; // it's just default
    adSize = EXIAD_PORTRAIT;
    return self;
}
// 
//
//
-(void)dealloc
{
    [super dealloc];
}
//
-(void)start:(int) _size
{
    if (!adView) {
        NSLog(@"starting iad...");
        reqAdSize = _size;
        [self performSelectorOnMainThread:@selector(unthread_start)
                               withObject:nil waitUntilDone:TRUE];
    } else {
        NSLog(@"already started");
    }
    return;
}
-(void)unthread_start
{
    NS_DURING
    NSLog(@"Iadnetwork start");
    // initialize ad view
    adView = [[[ADBannerView alloc] initWithFrame:CGRectZero] autorelease];
    CGRect original = [[mainView view] bounds];
    int w = (int)original.size.width;
    int h = (int)original.size.height;
    NSLog(@"mainview width: %d", w);
    switch (self.reqAdSize) {
        case EXIAD_PORTRAIT:
            NSLog(@"init ad in only portrait");
            adView.requiredContentSizeIdentifiers = [NSSet setWithObject:ADBannerContentSizeIdentifierPortrait];
            adView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierPortrait;
            break;
        case EXIAD_LANDSCAPE:
            NSLog(@"init ad in only landscape");
            adView.requiredContentSizeIdentifiers = [NSSet setWithObject:ADBannerContentSizeIdentifierLandscape];
            adView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierLandscape;
            break;
        case EXIAD_BOTH:
            NSLog(@"init ad in both of portrait and landscape");
            adView.requiredContentSizeIdentifiers = [NSSet setWithObjects: ADBannerContentSizeIdentifierPortrait,ADBannerContentSizeIdentifierLandscape,nil];
            NSLog(@"mainview height: %d", h);
            if (w<h) {
                adView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierPortrait;
            } else {
                adView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierLandscape;
            }
            break;            
        default:
            Assert(FALSE, @"unknown requireContentSize");
            adView.requiredContentSizeIdentifiers = [NSSet setWithObjects: ADBannerContentSizeIdentifierPortrait,ADBannerContentSizeIdentifierLandscape,nil];
            adView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierPortrait;
            self.reqAdSize = EXIAD_BOTH;
            NSLog(@"force to BOTH...");
            break;
    }
    // calculate offset to hide banner
    offsetX = ((w>h)?w:h)*2;
    NSLog(@"offsetX = %d",offsetX);

    // hide banner at begin
    adView.frame = CGRectOffset(adView.frame, x, y); // locate banner
    adView.frame = CGRectOffset(adView.frame, offsetX, y); // hide

    // set delegate of ADBannerView
    adView.delegate = self;

    // add ad banner view into main view as sub
    [[mainView view] addSubview:adView];
    NS_HANDLER
    NSLog(@"%@", [localException callStackSymbols]);
    NS_ENDHANDLER
    return;
}
-(void)quit
{
    [self performSelectorOnMainThread:@selector(unthread_quit)
                           withObject:nil waitUntilDone:TRUE];
    return;
}
-(void)unthread_quit
{
    NS_DURING
    NSLog(@"quit");
    adView.delegate = nil;
    [adView removeFromSuperview];
    //[adView release];
    adView = nil;
    NS_HANDLER
    NSLog(@"%@", [localException callStackSymbols]);
    NS_ENDHANDLER
    return;
}
//
//
-(void)setAttr:(int) ori
{
    self.adSize = ori;
    [self performSelectorOnMainThread:@selector(unthread_setAttr)
                           withObject:nil waitUntilDone:NO];
    return;
}
-(void)unthread_setAttr
{
    NS_DURING
    switch (self.adSize) {
        case EXIAD_PORTRAIT:
            NSLog(@"setAttr Adsize=Portrait");
            adView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierPortrait;
            break;
        case EXIAD_LANDSCAPE:
            NSLog(@"setAttr Adsize=Landscape");
            adView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierLandscape;
            break;
        default:
            Assert(FALSE, @"unknown adsize");
            adView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierPortrait;
            NSLog(@"force portrait");
            self.adSize = EXIAD_PORTRAIT;
            break;
    }
    NS_HANDLER
    NSLog(@"%@", [localException callStackSymbols]);
    NS_ENDHANDLER
    return;
}
-(void)setPosition:(int) _x y:(int) _y
{

    self.x = _x;
    self.y = _y;
    [self performSelectorOnMainThread:@selector(unthread_setPosition)
                           withObject:nil waitUntilDone:NO];
    return;
}
-(void)unthread_setPosition
{
    NS_DURING
    NSLog(@"set new potision at %d:%d",self.x,self.y);
    if (isVisible) {
        adView.frame = CGRectMake(self.x, self.y, adView.frame.size.width, adView.frame.size.height);
    } else {
        adView.frame = CGRectMake(offsetX, self.y, adView.frame.size.width, adView.frame.size.height);
    }
    NS_HANDLER
    NSLog(@"%@", [localException callStackSymbols]);
    NS_ENDHANDLER
    
    return;
    
}

-(void)switchad:(BOOL) flag
{
    on = flag;
    if (flag) {
        NSLog(@"switch on. make ad visible if possible");
        [self displayad];
    } else {
        NSLog(@"switch off. make ad unvisible even if possible");
        [self hidead];        
    }
    return;
}
- (void)displayad
{
    [self performSelectorOnMainThread:@selector(unthread_displayad)
                           withObject:nil waitUntilDone:NO];
    return;
}
- (void)unthread_displayad
{
    NS_DURING
    if (isAvailable && !isVisible && on) {
        // show banner, when banner is available
        NSLog(@"making ad visible...");
        if (animation) {
            [UIView beginAnimations:@"animateAdBannerOn" context:NULL];
            [UIView setAnimationDelay:1.0];  // 1秒後にアニメーションを開始する            
        }
        adView.frame = CGRectMake(x, y, adView.frame.size.width, adView.frame.size.height); // move ad banner from right to left, so that banner can be visible at the view
        if (animation) {
            [UIView commitAnimations];
        }
        // set visible flag
        isVisible = YES;
    }
    NS_HANDLER
    NSLog(@"%@", [localException callStackSymbols]);
    NS_ENDHANDLER
    return;
}
- (void)hidead
{
    [self performSelectorOnMainThread:@selector(unthread_hidead)
                           withObject:nil waitUntilDone:NO];
    return;
}
- (void)unthread_hidead
{
    NS_DURING
    if (isVisible) {
        // hidden ad banner, even ad is available
        if (animation) {
            [UIView beginAnimations:@"animateAdBannerOff" context:NULL];
        }
        adView.frame = CGRectMake(offsetX, y, adView.frame.size.width, adView.frame.size.height); // move ad banner to right, so that the banner is hidden
        if (animation) {
            [UIView commitAnimations];
        }
        // reset visible flag
        isVisible = NO;
    }
    NS_HANDLER
    NSLog(@"%@", [localException callStackSymbols]);
    NS_ENDHANDLER
    return;
}
#pragma MARK handling bannder events
//
// will work on main thread
// will be called, when failed read ad
- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError * )error{
    NSLog(@"didFailToReceiveAdWithError error: %@", error);
    [self hidead];
    isAvailable = NO;
    [adsdelegate didFailToReceiveAd:self];
    return;
}

// will be called, when ad is read
- (void)bannerViewDidLoadAd:(ADBannerView *)banner{
    NSLog(@"bannerViewDidLoadAd");
    isAvailable = YES;
    if (!adsdelegate) {
        [self displayad];
    } else {
        [adsdelegate adViewDidReceiveAd:self];
    }
    return;
}

// allow action trigger
- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner
               willLeaveApplication:(BOOL)willLeave
{
    NSLog(@"Banner view is beginning an ad action");
    BOOL shouldExecuteAction = [self allowActionToRun];
    if (!willLeave && shouldExecuteAction)
    {
        // TODO: if you want to stop some services while user interacts ad, make code here.
    }
    return shouldExecuteAction;
}
// called after iad action
- (void)bannerViewActionDidFinish:(ADBannerView *)banner {
    NSLog(@"bannerViewActionDidFinish");
    // TODO: recover services that you stopped before
    return;
}
// ads extended function
void iadnetwork_setDelegate(id ads) {
    adsdelegate = ads;
    return;
}
#pragma MARK RUBY interface
//
// This C function interfaces with the Objective C start function.
//
id iadnetwork_start(int size) {
    NS_DURING
    EXIAD_CHECK_IOS_VER;
    // adsize EXIAD_PORTRAIT:portrait
    //        EXIAD_LANDSCAPE:landscape
    //        EXIAD_PORTRAIT+EXIAD_LANDSCAPE:both
    NSLog(@"iaddnetwork_start");
    if (iadn){
        // quit once
        iadnetwork_quit();
    }
    // Initialize the Objective C class.
    NSLog(@"not started yet. strating force...");
    iadn = [[Iadnetwork alloc] init];
    [iadn start:size];
    NS_HANDLER
    NSLog(@"%@", [localException callStackSymbols]);
    NS_ENDHANDLER
    return iadn;
}
void iadnetwork_setSize(int adsize) {
    NS_DURING
    EXIAD_CHECK_IOS_VER;
    // adsize EXIAD_PORTRAIT:portrait EXIAD_LANDSCAPE:landscape
    NSLog(@"iadnetwork_attribute");
    if (iadn) {
        NSLog(@"setting ad as %@",(adsize==0)?@"portait":@"landscape");
    } else {
        NSLog(@"not started yet. strating force...");
        iadnetwork_start(EXIAD_BOTH);
    }
    [iadn setAttr:adsize];
    NS_HANDLER
    NSLog(@"%@", [localException callStackSymbols]);
    NS_ENDHANDLER
    return;
}
void iadnetwork_setPos(int _x, int _y)
{
    NS_DURING
    EXIAD_CHECK_IOS_VER;
    NSLog(@"iadnetwork_setPosition");
    if (!iadn) {
        NSLog(@"not started yet. strating force...");
        iadnetwork_start(EXIAD_BOTH);
    }
    [iadn setPosition:_x y:_y];
    NS_HANDLER
    NSLog(@"%@", [localException callStackSymbols]);
    NS_ENDHANDLER
    return;
}
void iadnetwork_show(void){
    NS_DURING
    EXIAD_CHECK_IOS_VER;
    NSLog(@"iadnetwork_show");
    if (iadn) {
        NSLog(@"started already. ignored request.");
    } else {
        NSLog(@"not started yet. strating force...");
        iadnetwork_start(EXIAD_BOTH);
    }
    [iadn switchad:YES];
    NS_HANDLER
    NSLog(@"%@", [localException callStackSymbols]);
    NS_ENDHANDLER
    return;
}
void iadnetwork_hide(void){
    NS_DURING
    EXIAD_CHECK_IOS_VER;
    NSLog(@"iadnetwork_hide");
    if (iadn) {
        NSLog(@"started already");
        [iadn switchad:NO];
    } else {
        NSLog(@"not started yet. nothing to do");
    }
    NS_HANDLER
    NSLog(@"%@", [localException callStackSymbols]);
    NS_ENDHANDLER
    return;
}
void iadnetwork_quit(void) {
    NS_DURING
    EXIAD_CHECK_IOS_VER;
    NSLog(@"iaddnetwork_quit");
    if (iadn) {
        [iadn quit];
        [iadn release];
    }
    iadn =  nil;
    NS_HANDLER
    NSLog(@"%@", [localException callStackSymbols]);
    NS_ENDHANDLER
return ;
}
//
#pragma mark check_ios
//
void iadnetwork_check_ios(void){
    debug = rho_conf_getBool("EXiadDebug");
    animation = !rho_conf_getBool("EXiadNoAnimation");
    NSLog(@"detected iOS Ver.%@",[[UIDevice currentDevice] systemVersion]);
    if (([[[UIDevice currentDevice] systemVersion] compare:@"4.0" options:NSNumericSearch] != NSOrderedAscending)) 
    {
        NSLog(@"OK");
        // OK
        iadn_valid_os = true;
    } else {
        // NG
        iadn_valid_os = false;
        NSLog(@"API will be ignored...");
        return;
    }

    return;
}
//
#pragma mark experimental function
//
int iadnetwork_isVisible(void){
    NSLog(@"isVisible is experimental");
    if (iadn) {
        if ([iadn isVisible]) {
            return 1;
        }
    }
    
    return 0;
}
int iadnetwork_isAvailable(void){
    NSLog(@"isAvailable is experimental");
    if (iadn) {
        if ([iadn isAvailable]) {
            return 1;
        }
    }
    
    return 0;
}

@end
