/*
 * Copyright (c) 2012 HAYAKAWA Takashi
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * http://ackcell.com
 */
#import <iAd/iAd.h>
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

// below must be same as iadnetwork.rb
#define EXIAD_PORTRAIT 1
#define EXIAD_LANDSCAPE 2
#define EXIAD_BOTH 3
//

// Debug
#define NSLog(...) {\
    if (debug) {\
        NSLog(@"IAD %s:%d",__FUNCTION__,__LINE__);\
        NSLog(__VA_ARGS__);\
    } else {\
    ;\
    }\
}
#define Assert(cond,...) {\
    if (debug) {\
        NSLog(@"IAD %s:%d",__FUNCTION__,__LINE__);\
        NSAssert(cond,__VA_ARGS__);\
    } else {\
        NSLog(__VA_ARGS__);\
    }\
}

#define EXIAD_CHECK_IOS_VER  if (!iadn_valid_os) { return; }

@interface Iadnetwork : UIViewController <ADBannerViewDelegate> {
    ADBannerView *adView; // ad banner view
    BOOL isAvailable; // ad banner is available or not
    BOOL isVisible; // ad banner is visible on the view or not
    BOOL on; // requested to show ad banner or not
    BOOL allowActionToRun; // allow action to run by iad click
    int x; // banner location x
    int y; // banner location y
    int offsetX; // offset to hide banner
    int adSize;
    int reqAdSize;
}

@property (nonatomic, retain) ADBannerView *adView;
@property (nonatomic, assign) BOOL isVisible;
@property (nonatomic, assign) BOOL isAvailable;
@property (nonatomic, assign) BOOL on;
@property (nonatomic, assign) BOOL allowActionToRun;
@property (nonatomic, assign) int x;
@property (nonatomic, assign) int y;
@property (nonatomic, assign) int reqAdSize;
@property (nonatomic, assign) int adSize;

@end

void iadnetwork_quit(void);

