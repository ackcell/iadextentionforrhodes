/* iadnetwork.i */


%module Iadnetwork
%{
#include "ruby/ext/rho/rhoruby.h"

extern void iadnetwork_start(int size);
extern void iadnetwork_show(void);
extern void iadnetwork_hide(void);
extern void iadnetwork_quit(void);
extern void iadnetwork_setSize(int adsize);
extern void iadnetwork_setPos(int _x, int _y);
extern int iadnetwotk_isAvailable(void);
extern int iadnetwotk_isVisible(void);
extern void iadnetwotk_check_ios(void);

#define start iadnetwork_start 
#define show iadnetwork_show 
#define hide iadnetwork_hide 
#define quit iadnetwork_quit
#define setSize iadnetwork_setSize
#define setPos iadnetwork_setPos
#define isAvailable iadnetwork_isAvailable
#define isVisible iadnetwork_isVisible

%}

%rename("isAvailable?") isVisible();

%typemap(out) int isVisible 
 "$result = ($1 != 0) ? Qtrue : Qfalse;";

%rename("isAvailable?") isAvailable();

%typemap(out) int isAvailable 
 "$result = ($1 != 0) ? Qtrue : Qfalse;";

extern void start(int size);
extern void show(void);
extern void hide(void);
extern void quit(void);
extern void setSize(int adsize);
extern void setPos(int _x, int _y);
extern int isAvailable(void);
extern int isVisible(void);
