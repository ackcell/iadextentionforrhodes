=begin
/*
 * Copyright (c) 2012 HAYAKAWA Takashi
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * http://ackcell.com
 */
=end

require 'rho/rhocontroller'
require 'helpers/application_helper'
require 'helpers/browser_helper'
require 'iadnetwork'

include BrowserHelper
include ApplicationHelper
include Iadnetwork

describe "iAD Typically Case" do
  before(:each) do
    Iadnetwork.quit
    System::set_screen_rotation_notification(nil, nil)
  end
  it "typically" do

    Iadnetwork.start(iad_portrait+iad_landscape)

    Iadnetwork.setPos(0,100)

    Iadnetwork.show

    puts "wait until ad visible"
    loopcnt = 0
    while (!Iadnetwork.isVisible)
      loopcnt = loopcnt + 1
      puts("sleep until ad...")
      sleep(3) # sleep 3sec to wait ad avaiable and visible
      if (loopcnt == 15)
        Alert.show_popup 'ad inventory has been unavailable for while. quitting test...'
      end
      #pending "something happen. no ad avaiable for long time." do
        loopcnt.should_not == 15  # do not repeat more
      #end
    end
    Iadnetwork.hide
    sleep(3)
    Iadnetwork.show
    sleep(1)

    Iadnetwork.setPos(30,200)
    sleep(1)

    Iadnetwork.setPos(0,300)
    sleep(1)

    Iadnetwork.setPos(0,0)
    sleep(1)
    Iadnetwork.setSize(iad_landscape)

    sleep(1)
    Iadnetwork.setSize(iad_portrait)

    sleep(1)
    Iadnetwork.quit

  end
  after(:each) do
    Iadnetwork.quit
    System::set_screen_rotation_notification(nil, nil)
  end
end
