=begin
/*
 * Copyright (c) 2012 HAYAKAWA Takashi
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * http://ackcell.com
*/
=end

require 'rho/rhocontroller'
require 'helpers/application_helper'
require 'helpers/browser_helper'
require 'iadnetwork'

include BrowserHelper
include ApplicationHelper
include Iadnetwork

describe "iAD API" do
  before(:each) do
    Iadnetwork.quit
    System::set_screen_rotation_notification(nil, nil)
  end

  it "quit 1" do
    Iadnetwork.quit
    Iadnetwork.isVisible.should == false
  end
  it "quit 2" do
    Iadnetwork.quit
    Iadnetwork.isAvailable.should == false
  end
  it "start" do
    Iadnetwork.start(iad_portrait+iad_landscape)
  end
  it "start portrait" do
    Iadnetwork.start(iad_portrait)
  end
  it "start landscape" do
    Iadnetwork.start(iad_landscape)
  end
  it "set position" do
    Iadnetwork.setPos(0,0)
  end
  it "set position(0,100)" do
    Iadnetwork.setPos(0,100)
  end
  it "set adsize portrait" do
    Iadnetwork.setSize(iad_portrait)
  end
  it "set adsize landscape" do
    Iadnetwork.setSize(iad_landscape)
  end
  it "show ad" do
    Iadnetwork.show
  end
  it "hide ad" do
    Iadnetwork.show
    Iadnetwork.hide
  end

  after(:each) do
    Iadnetwork.quit
    System::set_screen_rotation_notification(nil, nil)
  end
end

