require 'rho/rhoapplication'

class AppApplication < Rho::RhoApplication
  def initialize
    # Tab items are loaded left->right, @tabs[0] is leftmost tab in the tab-bar
    # Super must be called *after* settings @tabs!
    @tabs = nil
    #To remove default toolbar uncomment next line:
    #@@toolbar = nil
    super

    # Uncomment to set sync notification callback to /app/Settings/sync_notify.
    # SyncEngine::set_objectnotify_url("/app/Settings/sync_notify")
    SyncEngine.set_notification(-1, "/app/Settings/sync_notify", '')
=begin
    # Native Tab
    tabs = [
      { :label => 'tab 0', :action => '/app',         # 0
        :icon => "/public/images/iphone/disclosure.png" }, 
      { :label => 'tab 1', :action => '/app',     # 1  
        :icon => "/public/images/iphone/disclosure.png" },
    ]
    Rho::NativeTabbar.create(:tabs => tabs)
    Rho::NativeTabbar.switch_tab(0) 
    WebView.navigate('/app',1)
=end
  end
end
