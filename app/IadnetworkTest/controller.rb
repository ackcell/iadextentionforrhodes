=begin
/*
 * Copyright (c) 2012 HAYAKAWA Takashi
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * http://ackcell.com
 */
=end
require 'rho/rhocontroller'
require 'helpers/application_helper'
require 'helpers/browser_helper'
require 'iadnetwork'

class IadnetworkTestController < Rho::RhoController
  include BrowserHelper
  include ApplicationHelper
  include Iadnetwork

  
  def index
    # typically call sequence
    render :back => '/app'
  end

  def setcallback
    System::set_screen_rotation_notification('/app/IadnetworkTest/screenRotateCallback', "" )
    #System::set_screen_rotation_notification(url_for(:action => :screenRotateCallback), "" )
    render :action => :index, :back => '/app'
  end
  def unsetcallback
    System::set_screen_rotation_notification(nil, nil )
    render :action => :index, :back => '/app'
  end
  def startad
    Iadnetwork.start(iad_portrait+iad_landscape)
    render :action => :index, :back => '/app'
  end
  def setposition
    render :back => url_for(:action => :index)
  end
  def getposition
    x = @params['getposition']['x']
    y = @params['getposition']['y']
    unless x =~ /\d+/ && y =~ /\d+/
      Alert.show_popup 'Enter number'
      render :action => :setposition
    else
      Iadnetwork.setPos(x.to_i, y.to_i)
      render :action => :index, :back => '/app'
    end
  end
  def resetposition
    Iadnetwork.setPos(0, 0)
    render :action => :index, :back => '/app'
  end
  def setsizeportrait
    Iadnetwork.setSize(iad_portrait)
    render :action => :index, :back => '/app'
  end
  def setsizelandscape
    Iadnetwork.setSize(iad_landscape)
    render :action => :index, :back => '/app'
  end
  def startadportrait
    Iadnetwork.start(iad_portrait)
    render :action => :index, :back => '/app'
  end
  def startadlandscape
    Iadnetwork.start(iad_landscape)
    render :action => :index, :back => '/app'
  end
  def endad
    Iadnetwork.quit
    render :action => :index, :back => '/app'
  end
  def display
    Iadnetwork.show
    render :action => :index, :back => '/app'
  end
  def hide
    Iadnetwork.hide
    render :action => :index, :back => '/app'
  end
  
  # for screen rotation

  def screenRotateCallback
    puts "Screen Rotated W["+@params['width']+"]xH["+@params['height']+"] Degrees["+@params['degrees']+"]"    
    h = @params['height'].to_i
    w = @params['width'].to_i
    if (h>w)
      # portrait
      puts "portrait"
      Iadnetwork.setSize(iad_portrait)
    else
      # landscape
      puts "landscape"
      Iadnetwork.setSize(iad_landscape)
    end
  end
end
